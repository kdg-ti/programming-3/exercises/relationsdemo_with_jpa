package be.kdg.java3.relationsdemo.repository;

import be.kdg.java3.relationsdemo.domain.School;

import java.util.List;

public interface SchoolRepository {
    List<School> findAll();
    School findById(int id);
    School createSchool(School school);//returns a School with autogenerated id
    void updateSchool(School school);
    void deleteSchool(int id);
}
